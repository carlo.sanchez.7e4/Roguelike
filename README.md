# RogueLike
El jugador haurà de sobreviure 30 segons als enemics que aniran spawnejant. Començarà amb 20 bales d'una de les 5 armes del joc.
Cada enemic té un 30% de probabilitat de dropejar una quantitat aleatoria (varia segons l'item) de bales d'alguna de les armes del joc o un cor per recuperar vida.
Les diferents armes són:
- Fire Crystal:
	Velocitat i cadencia de bales standard. 30 de mal per dispar.
- Ice Crystal:
	Velocitat i cadencia de bales standard. 25 de mal per dispar i 5 de defensa.
- Earth Crystal:
	Velocitat i cadencia de bales lenta. 15 de mal per dispar i 15 de defensa.
- Lightning Crystal:
	Velocitat i cadencia de bales molt ràpida. 15 de mal per dispar.
- Shadow Crystal:
	Velocitat i cadencia de bales molt lenta. 50 de mal.

Quan el player mori o hagin passat 30 segons, s'anira a una pantalla de resultat on indicarà si s'ha guanyat o perdut i quina puntuació s'ha aconseguit.
La puntuació funciona de la següent forma:
	Per item recollit (5 punts), per dispar donat (segons mal inflingit) i per enemic eliminat (segons l'enemic).

# Controls
WASD - Moviment
Mouse - Apuntar
Click Esquerre - Dispar
X, C o Roda del Ratolí - Canviar d'arma