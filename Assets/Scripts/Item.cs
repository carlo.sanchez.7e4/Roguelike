using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public ItemSO item;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            int amount = Random.Range(item.minAmount, item.maxAmount);
            collision.gameObject.GetComponent<PlayerData>().inventory.AddItem(item, amount);
            GameManager.Instance.SumScore(5);
            Destroy(gameObject);
        }
    }
}
