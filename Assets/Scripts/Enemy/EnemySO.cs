using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base Stats of an Enemy
/// </summary>
[CreateAssetMenu(fileName = "Enemy Config", menuName = "ScriptableObject/Enemy Config")]
public class EnemySO : ScriptableObject
{
    public float HP;
    public float dmg;
    public int points;
    public int speed;
    public int dropChance;
}
