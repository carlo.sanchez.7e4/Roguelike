using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minion : EnemyController
{
    private Vector2 targetPosition;
    

    void Update()
    {
        MoveTowardsTarget();
    }


    private void MoveTowardsTarget()
    {
        targetPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, enemySO.speed * Time.deltaTime);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerData>().TakeDamage(enemySO.dmg);
            Destroy(gameObject);
        }
    }
}
