using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    [SerializeField]
    public EnemySO enemySO;
    [SerializeField]
    private float HP;
    public Rigidbody2D rb;
    public DropablesSO dropables;

    // Start is called before the first frame update
    void Start()
    {
        HP = enemySO.HP;
    }

    public void TakeDamage(float dmg)
    {
        if (HP > 0)
        {
            HP -= dmg;
            if (HP <= 0)
            {
                Destroy(gameObject);
                GameManager.Instance.SumScore(enemySO.points);
            }
        }

    }

    private void OnDestroy()
    {
        int dropChance = Random.Range(0, 100);
        if (dropChance <= enemySO.dropChance)
        {
        int index = Random.Range(0, dropables.drops.Count - 1);
        Instantiate(dropables.drops[index].GetComponent<Rigidbody2D>(), transform.position, transform.rotation);
        }

    }
}
