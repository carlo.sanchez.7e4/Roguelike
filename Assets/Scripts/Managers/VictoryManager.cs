using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VictoryManager : MonoBehaviour
{
    private static VictoryManager _instance;

    public static VictoryManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Victory Manager is NULL!");
            }

            return _instance;
        }


    }

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {

            Destroy(this.gameObject);
        }

        _instance = this;

    }

    public Text score;

    public Button _playAgainButton;


    private void Start()
    {
        _playAgainButton.onClick.AddListener(ToHomeScreen);
        score.text = string.Format("Score: {0}", PlayerPrefs.GetInt("score"));
    }

    private void ToHomeScreen()
    {
        SceneManager.LoadScene(0);
    }
}
