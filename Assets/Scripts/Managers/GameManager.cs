using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }

            return _instance;
        }


    }

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {

            Destroy(this.gameObject);
        }

        _instance = this;

    }

    public GameObject player;
    public Text scoreText;
    public Text ammoText;
    public ItemSO startingItem;

    private PlayerData pd;
    private PlayerMovement pm;
    private int score = 0;

    private Vector3 mousePos;
    private Vector3 mousePosPixels;
    private Vector3 playerPos;
    private float angle;

    // Start is called before the first frame update
    void Start()
    {
        pd = player.GetComponent<PlayerData>();
        pd.inventory.AddItem(startingItem, 20);
        pm = player.GetComponent<PlayerMovement>();

    }

    // Update is called once per frame
    void Update()
    {
        ShowAmmo();
        UpdateScore();
        RotatePlayer();

        if (pd.HP <= 0)
        {
            Death();
        }
    }

    private void RotatePlayer()
    {
        mousePosPixels = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePosPixels);

        playerPos.x = player.transform.position.x;
        playerPos.y = player.transform.position.y;
        playerPos.z = 2;

        Vector2 shootVector = mousePos - playerPos;

        angle = Mathf.Atan2(shootVector.y, shootVector.x) * Mathf.Rad2Deg;
        if (angle < 0) angle += 360;
        pd.animator.SetFloat("angle", angle);
    }

    private void Death()
    {
        pd.animator.SetBool("death", true);
        pm.speed = 0;
    }

    private void UpdateScore()
    {
        scoreText.text = score.ToString();
    }

    private void ShowAmmo()
    {
        ammoText.text = string.Format("Ammo: {0}", pd.equipedItem.amount);
    }

    public void SumScore(int points)
    {
        score += points;
    }

    private void OnApplicationQuit()
    {
        pd.inventory.inventory.Clear();
    }

    public void Victory()
    {
        PlayerPrefs.SetInt("score", score);
        SceneManager.LoadScene("VictoryScreen");
    }

    public void GameOver()
    {
        PlayerPrefs.SetInt("score", score);
        SceneManager.LoadScene("GameOver");
    }
}
