using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "ScriptableObject/Inventory")]
public class InventorySO : ScriptableObject
{
    public List<InventorySlot> inventory = new List<InventorySlot>();
    public void AddItem(ItemSO _item, int _amount)
    {
        bool hasItem = false;
        for (int i = 0; i < inventory.Count; ++i)
        {
            if (inventory[i].item == _item)
            {
                inventory[i].AddAmount(_amount);
                hasItem = true;
                break;
            }
        }
        if (!hasItem)
        {
            inventory.Add(new InventorySlot(_item, _amount));
        }
    }

}

[System.Serializable]
public class InventorySlot
{
    public ItemSO item;
    public int amount;
    public InventorySlot(ItemSO _item, int _amount)
    {
        item = _item;
        amount = _amount;
    }
    public void AddAmount(int value)
    {
        amount += value;
    }
}

