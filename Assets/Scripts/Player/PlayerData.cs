using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{
    public float Speed;
    public float HP;
    public float maxHP;

    public InventorySO inventory;

    private HealthBar healthBar;

    public InventorySlot equipedItem;
    public GameObject equipedItemImage;
    public GameObject ammoTextPanel;

    public GameObject firePoint;
    public Animator animator;


    // Start is called before the first frame update
    void Start()
    {
        HP = maxHP;
        healthBar = GameObject.Find("HealthBar").GetComponent<HealthBar>();
        healthBar.SetMaxHP(maxHP);
    }

    // Update is called once per frame
    void Update()
    {
        if (inventory.inventory.Count == 0)
        {
            equipedItemImage.SetActive(false);
            firePoint.SetActive(false);
            ammoTextPanel.SetActive(false);
        }
        else if (inventory.inventory.Count == 1)
        {
            equipedItemImage.SetActive(true);
            firePoint.SetActive(true);
            ammoTextPanel.SetActive(true);

            equipedItem = inventory.inventory[0];
            equipedItemImage.GetComponent<Image>().sprite = inventory.inventory[0].item.img;
        }

        SwitchEquip();

    }

    private void SwitchEquip()
    {
        if (inventory.inventory.Count > 1)
            if (Input.GetKeyDown(KeyCode.X) || Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                int index = inventory.inventory.IndexOf(equipedItem);
                if (index == 0)
                {
                    equipedItem = inventory.inventory[inventory.inventory.Count - 1];
                    equipedItemImage.GetComponent<Image>().sprite = inventory.inventory[inventory.inventory.Count - 1].item.img;
                }
                else
                {
                    equipedItem = inventory.inventory[index - 1];
                    equipedItemImage.GetComponent<Image>().sprite = inventory.inventory[index - 1].item.img;
                }
            }
            else if (Input.GetKeyDown(KeyCode.C) || Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                int index = inventory.inventory.IndexOf(equipedItem);
                if (index == inventory.inventory.Count - 1)
                {
                    equipedItem = inventory.inventory[0];
                    equipedItemImage.GetComponent<Image>().sprite = inventory.inventory[0].item.img;
                }
                else
                {
                    equipedItem = inventory.inventory[index + 1];
                    equipedItemImage.GetComponent<Image>().sprite = inventory.inventory[index + 1].item.img;
                }
            }
    }


    public void TakeDamage(float dmg)
    {
        HP -= dmg - equipedItem.item.defBonus;
        healthBar.SetHealth(HP);
    }

    public void Heal(int heal)
    {
        HP += heal;
        if (HP > maxHP)
        {
            HP = maxHP;
        }
        healthBar.SetHealth(HP);
    }

    public void AlertObservers(string message)
    {
        if (message.Equals("DeathAnimEnded"))
        {
            GameManager.Instance.GameOver();
        }
    }
}
