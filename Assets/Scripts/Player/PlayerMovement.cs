using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    private float moveHorizontal;
    private float moveVertical;
    public float speed;

    [SerializeField]
    public PlayerData PD;
    public Rigidbody2D RB;



    // Start is called before the first frame update
    void Start()
    {
        speed = PD.Speed;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        movement();
    }

    private void movement()
    {
        /*
        if (Input.GetAxis("Horizontal") > 0)
        {
            spriteRenderer.flipX = false;
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            spriteRenderer.flipX = true;
        }
        */

        moveHorizontal = Input.GetAxis("Horizontal") * speed;
        moveVertical = Input.GetAxis("Vertical") * speed;

        RB.velocity = new Vector2(moveHorizontal, moveVertical);
    }

}
