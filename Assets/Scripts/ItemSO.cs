using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equip Object", menuName = "ScriptableObject/Item")]
public class ItemSO : ScriptableObject
{
    public GameObject prefab;
    public GameObject bullet;
    public Sprite img;
    [TextArea(15, 20)]
    public string description;

    public int atkBonus;
    public int defBonus;
    public int speed;
    public float fireRate;
    public int minAmount;
    public int maxAmount;
}

