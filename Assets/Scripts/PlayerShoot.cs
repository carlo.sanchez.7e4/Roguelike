using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Vector3 mouse_pos;
    public Vector3 mouse_pos_pixels;
    private Vector3 firePoint;
    private float angle;
    private Rigidbody2D rbFirePoint;
    private PlayerData pd;
    private float nextFire = 0.0f;

    public Camera cam;
    public GameObject bullet;

    void Start()
    {
        rbFirePoint = GetComponent<Rigidbody2D>();
        pd = GameObject.Find("Player").GetComponent<PlayerData>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && Time.time > nextFire)
        {
            nextFire = Time.time + pd.equipedItem.item.fireRate;
            if (pd.equipedItem.amount > 0)
            {
                pd.equipedItem.amount -= 1;
                mouse_pos_pixels = Input.mousePosition;
                mouse_pos = cam.ScreenToWorldPoint(mouse_pos_pixels);

                firePoint.x = transform.position.x;
                firePoint.y = transform.position.y;
                firePoint.z = 2;

                Vector2 shootVector = mouse_pos - firePoint;

                angle = Mathf.Atan2(shootVector.y, shootVector.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0, 0, angle);

                rbFirePoint = Instantiate(bullet.GetComponent<Rigidbody2D>(), firePoint, transform.rotation);
                shootVector.Normalize();
                rbFirePoint.velocity = shootVector * pd.equipedItem.item.speed;
            }        


        }
    }

}
