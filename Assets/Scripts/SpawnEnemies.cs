using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnEnemies : MonoBehaviour
{
    private float spawnRate = 1.5f;
    private float spawnTime = 0f;

    private float spawnDuration = 30f;
    private float spawnStart;

    public GameObject enemy;

    public Vector2 randomX;
    public Vector2 randomY;


    private void Start()
    {
        spawnStart = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        Spawn();
        if (Time.time > spawnStart + spawnDuration)
        {
            GameManager.Instance.Victory();
        }
    }

    private void Spawn()
    {
        if (Time.time > spawnTime)
        {
            Instantiate(enemy, new Vector3(Random.Range(randomX.x, randomX.y), Random.Range(randomY.x, randomY.y), 0), Quaternion.identity);
            spawnTime = Time.time + spawnRate;
        }
    }
}
