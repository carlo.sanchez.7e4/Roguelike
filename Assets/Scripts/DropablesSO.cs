using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Drop List", menuName = "ScriptableObject/Drop List")]
public class DropablesSO : ScriptableObject
{
    public List<GameObject> drops = new List<GameObject>();
}
